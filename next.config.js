const withTM = require('next-transpile-modules')(['three']); // pass the modules you would like to see transpiled

const config = {
  reactStrictMode: true,

  webpack: (config, options) => {
    const { isServer } = options;

    config.module.rules.push({
      test: /\.(ogg|mp3|m4a|m4v|wav|mpe?g)$/i,
      exclude: config.exclude,
      use: [
        {
          loader: "file-loader",
          options: {
            outputPath: "assets/audio/",
            outputPath: `${isServer ? '../' : ''}static/audio/`,
            name: '[name]-[hash].[ext]',
          },
        },
      ],
    });

    return config;
  },
}

module.exports = withTM(config)
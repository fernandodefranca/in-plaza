import { isServer } from '../utilities/is-server'

import { sceneCinemateca } from "../scenes/cinemateca";
import { sceneSe } from "../scenes/se";
import { sceneSaoBento } from "../scenes/sao-bento";
import { sceneCamposEliseos } from "../scenes/campos-eliseos";
import { sceneTransportes } from "../scenes/transportes";
import { sceneUrbanismo } from "../scenes/urbanismo";
import { sceneCreditos } from "../scenes/creditos";

import { setup as setupPanorama } from "../setup/setup";

const sceneConfig = {
  scenes: {
    initial: sceneCinemateca,
    cinemateca: sceneCinemateca,
    se: sceneSe,
    saoBento: sceneSaoBento,
    camposEliseos: sceneCamposEliseos,
    transportes: sceneTransportes,
    urbanismo: sceneUrbanismo,
    creditos: sceneCreditos,
  },
};

export const selectorName = 'THREE-SCENE'

let eventEmitterLocal = null
let sceneManagerLocal = null

export function usePanorama() {
  const isBrowser = !isServer()

  const setup = (eventEmitter) => {
    if (eventEmitterLocal || sceneManagerLocal || !isBrowser) {
      return new Promise((resolve) => {
        resolve({ eventEmitter: eventEmitterLocal, sceneManager:sceneManagerLocal })
      })
    }

    return setupPanorama(`#${selectorName}`, sceneConfig, eventEmitter)
      .then(({ eventEmitter, sceneManager }) => {
        eventEmitterLocal = eventEmitter
        sceneManagerLocal = sceneManager

        eventEmitter.on("HOTSPOT-CLICKED", (what) => {
          sceneManager.replaceScene(what);
        });

        return { eventEmitter, sceneManager }
      });
  }

  return {
    setup,
    eventEmitter: eventEmitterLocal,
    sceneManager: sceneManagerLocal,
  }
}
/* eslint-disable @next/next/no-img-element */
import styled from 'styled-components';
import logos from './logos.png'

const InfoContainer = styled.div`
  position: absolute;
  bottom: 0;
  left: 1rem;
  right: 1rem;
  padding: 0 0.5rem;  
`

const Logos = styled.div`
  padding: 0 0.5rem;
  background: #ccc;

  img {
    width: auto;
    height: auto;
    max-height: 96px;
    max-width: 100%;
  }
`;

const Padding = styled.div`
  margin-top: 1rem;
`

export const FooterInfo = ({children}) => (
  <InfoContainer>
    { children }
    { children && <Padding /> }
    <Logos>
      <img
        src={logos.src}
        alt="PROAC EDITAIS, CULTURA EM CASA, amigos da arte, SÃO PAULO GOVERNO DO ESTADO"
        />
    </Logos>
  </InfoContainer>
)

export const FooterItem = styled.button`
  display: block;

  font-size: 1.25rem;
  text-align: left;
  color: #fff;

  background: #333;
  border: none;

  & + & {
    margin-top: 0.25rem;
  }
`;

export const FooterItemButton = styled(FooterItem)`
  background: #c40404;
  cursor: pointer;
`

import styled from 'styled-components';

import { CloseIcon } from './close-icon';

export const InfoModalOverlay = styled.div`
  position: absolute;
  background: #000a;
  width: 100%;
  height: 100%;

  display:flex;
  justify-content:center;
  align-items:center;
`;

export const InfoModalContentWrapper = styled.div`
  height: 75vh;
  width: 75vh;
  position: relative;
  padding: 2rem;

  margin: 2rem;
  background: #fff;

  &.creditos {
    width: 95vw;
  }
`

export const InfoModalContent = styled.div`
  width: 100%;
  height: 100%;
  overflow: hidden;
  overflow-y: scroll;
`

export const VideoModalContentWrapper = styled.div`
  width: 75vw;
  height: 75vh;
  position: relative;
  padding: 2rem;

  margin: 2rem;
  background: #fff;
`

export const VideoModalContent = styled.div`
  width: 100%;
  height: 100%;
  overflow: hidden;
  overflow-y: scroll;
`

export const ModalCloseButton = styled.button`
  width: 2rem;
  height: 2rem;
  position: absolute;

  top: -1rem;
  right: -1rem;

  background: #c40404;
  border: 0;
  pointer: cursor;
`

export function InfoModal({handleCloseClick, children, className}) {
  return (
    <InfoModalOverlay onClick={handleCloseClick}>
      {
        <InfoModalContentWrapper className={className}>
          <ModalCloseButton onClick={handleCloseClick}>
            <CloseIcon />
          </ModalCloseButton>
          <InfoModalContent>
            {children}
          </InfoModalContent>
        </InfoModalContentWrapper>
      }
    </InfoModalOverlay>
  )
}

export function VideoModal({handleCloseClick, children}) {
  return (
    <InfoModalOverlay onClick={handleCloseClick}>
      {
        <VideoModalContentWrapper>
          <ModalCloseButton onClick={handleCloseClick}>
            <CloseIcon />
          </ModalCloseButton>
          <VideoModalContent>
            {children}
          </VideoModalContent>
        </VideoModalContentWrapper>
      }
    </InfoModalOverlay>
  )
}

import { AudioListener, Audio } from "three";

export function setupEnvironmentAudio(camera) {
  const audioListener = new AudioListener();
  camera.add(audioListener);
  const environmentAudio = new Audio(audioListener);

  return environmentAudio;
}

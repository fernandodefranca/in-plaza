import { WebGLRenderer } from "three";

export function setupRenderer(canvas, width, height) {
  const renderer = new WebGLRenderer({ canvas });
  renderer.setSize(width, height);
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2)); // Avoid crazy pixel densities

  return renderer;
}

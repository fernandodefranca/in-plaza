import '../styles/globals.css'
import styles from '../styles/Scene.module.css'
import { selectorName } from '../components/use-panorama'


function MyApp({ Component, pageProps }) {
  return (
    <div>
      <div id={selectorName} className={styles.container} />
      <Component {...pageProps} />
    </div>
  )
}

export default MyApp

import { useEffect, useState } from 'react';
import Modal from 'react-modal';
import mitt from "mitt";

import { usePanorama, selectorName } from '../components/use-panorama'
import scenes from '../scenes'
import { FooterInfo } from '../components/footer-info'
import { InfoModal, VideoModal } from '../components/modal'

Modal.setAppElement(`#${selectorName}`);

const eventEmitter = mitt();

export default function Home() {
  const { setup, } = usePanorama();
  const [sceneId, setSceneId] = useState('cinemateca');
  const SceneUI = scenes[sceneId] && scenes[sceneId].Ui;
  const hasSceneUI = !!SceneUI;
  const sceneInfo = scenes[sceneId] && scenes[sceneId].info;

  const [videoModalIsOpen, setVideoModalIsOpen] = useState(false);
  const [infoModalIsOpen, setInfoModalIsOpen] = useState(false);
  const [modalContent, setModalContent] = useState(null);
  function closeModal() {
    setVideoModalIsOpen(false);
  }

  useEffect(() => {
    if (!setup) return;

    setup(eventEmitter).then(({sceneManager}) => {
      setSceneId(sceneManager.currentScene.name)

      eventEmitter.on("SCENE-TRANSITION-STARTED", () => {
        setSceneId(null)
      });
  
      eventEmitter.on("SCENE-TRANSITION-DONE", () => {
        setSceneId(sceneManager.currentScene.name)
      });

      eventEmitter.on("NAV_BACK_HOME", () => {
        sceneManager.replaceScene("initial");
      })
    })

    eventEmitter.on('INFO', () => {
      setModalContent({type: 'info'})
      setInfoModalIsOpen(true);
    })

    eventEmitter.on("MAIN_VIDEO", (content) => {
      setModalContent({type: 'video', content})
      setVideoModalIsOpen(true);
    });

    eventEmitter.on("DOC_VIDEO", (content) => {
      setModalContent({type: 'video', content})
      setVideoModalIsOpen(true);
    });
  }, []);

  const handleInfoModalCloseClick = () => {
    setInfoModalIsOpen(false);
  }

  return (
    <div>
      <FooterInfo >
        {hasSceneUI && (<SceneUI eventEmitter={eventEmitter}/>)}
      </FooterInfo>
      {
        videoModalIsOpen && modalContent && modalContent.type === 'video' && (
          <VideoModal handleCloseClick={closeModal}>
            <iframe
              width="100%"
              height="100%"
              src={`${modalContent.content}?autoplay=1&rel=0&controls=0&showinfo=0&modestbranding=1`}
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
              title="Embedded youtube"
            />
          </VideoModal>
        )
      }
      {
        infoModalIsOpen && (
          <InfoModal handleCloseClick={handleInfoModalCloseClick} className={sceneId}>
            {
              sceneInfo.pt && 
              <div>{sceneInfo.pt}</div>
            }
            {
              sceneInfo.en &&
              <>
                <hr />
                <div><em>{sceneInfo.en}</em></div>
              </>
            }
            {
              typeof sceneInfo === 'function' && sceneInfo() /* Credits */
            }
          </InfoModal>
        )
      }
    </div>
  )
}

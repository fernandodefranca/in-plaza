import { useEffect } from 'react';
import Link from 'next/link'
import Head from 'next/head'
import { usePanorama } from '../components/use-panorama'

export default function Home() {
  const { sceneManager } = usePanorama();

  useEffect(() => {
    if (sceneManager) sceneManager.replaceScene('bixiga')
  }, [sceneManager]);

  return (
    <div style={{color:'red'}}>
      <Head>
        <title>Bixiga</title>
      </Head>
      
      <p>
        <Link href="/"><a>index page</a></Link>
      </p>
    </div>
  )
}

const path = require('path');
const glob = require("glob");
const sharp = require('sharp');
const buildConfig = require('../build-config');

const files = glob.sync('/Users/fernandodefranca/Dropbox/in-plaza/environments/**/*.+(jpg|jpeg|png)', { ignore: '**/*-compressed.jpg' })
const config = { ...buildConfig.image }

files.forEach((file) => {
  const { dir, name, ext} = path.parse(file);
  const { width, height, quality } = config;
  const targetFilePath = `${dir}/${name}-compressed.jpg`

  sharp(file)
    .resize(width, height)
    .jpeg({ mozjpeg: true, quality })
    .toFile(targetFilePath, (err, info) => {
      if (err) {
        throw new Error(`compress-images failed with file "${file}"`)
      } else {
        console.log("🗜 ", targetFilePath, Math.round(info.size/1024), "KB");
      }
    });
})
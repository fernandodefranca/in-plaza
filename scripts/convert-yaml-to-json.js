const fs = require('fs');
const path = require('path');
const glob = require("glob");
const YAML = require('yaml')

const files = glob.sync('./assets/**/data_yml.txt')

files
  .map(file => path.resolve(file))
  .map(filePath => ({
    filePath,
    data: YAML.parse(fs.readFileSync(filePath, 'utf8')),
  }))
  .forEach(({filePath, data}) => {
    const { dir } = path.parse(filePath);
    const jsonFilePath = path.resolve(dir, 'data.json');

    fs.writeFileSync(jsonFilePath, JSON.stringify(data, null, 2), 'utf8');
  })



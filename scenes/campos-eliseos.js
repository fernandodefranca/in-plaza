import { SceneModel } from "../models/scene-model";
import image from "../assets/03-campos-eliseos/texture-compressed.jpg";
import jsonData from "../assets/03-campos-eliseos/data.json";
import { FooterItemButton } from '../components/footer-info'

export const name = 'campos-eliseos';
export const sceneCamposEliseos = new SceneModel(name)
  .withImagePreload([image.src])
  .withMaterialFn(() => sceneCamposEliseos.materials[image.src])

export const Ui = ({eventEmitter}) => {
  const { videos } = jsonData.data;

  const handleInfoClick = () => {
    eventEmitter.emit('INFO', null)
  }

  const handleMainVideoClick = () => {
    eventEmitter.emit('MAIN_VIDEO', videos[0])
  }
  
  const handleDocVideoClick = () => {
    eventEmitter.emit('DOC_VIDEO', videos[1])
  }
  
  const handleNavBackClick = () => {
    eventEmitter.emit('NAV_BACK_HOME')
  }

  return (
    <>
      <FooterItemButton onClick={handleMainVideoClick}>
        {'>'} Assista / Watch - CAMPOS ELÍSEOS: Urbe-doc
      </FooterItemButton>
      <FooterItemButton onClick={handleDocVideoClick}>
        {'>'} Assista / Watch - CAMPOS ELÍSEOS: Planificação e apagamento / Planification and erasure
      </FooterItemButton>
      <FooterItemButton onClick={handleInfoClick}>
        + Info
      </FooterItemButton>
      <FooterItemButton onClick={handleNavBackClick}>
        {'<'} voltar  / back to IN-PLAZA
      </FooterItemButton>
    </>
  )
}

export const info = {
  pt: `Realizado em 1973, o documentário investiga a formação e a decadência de Campos Elíseos, bairro concebido como reduto pioneiro da elegância paulistana por volta de 1860 e gradualmente transformado, a partir dos anos 30, no cenário da criminalidade conhecido por Boca do Lixo. Passeando por imagens daquela atualidade, o diretor explora a arquitetura dos casarões do baronato do café, retratando a prosperidade das famílias que habitaram ali no passado, muitas delas arruinadas depois da crise econômica mundial de 1929.`,
  en: `Shot in 1973, the documentary investigates the formation and decay of Campos Elíseos, a neighborhood conceived as a pioneering stronghold of São Paulo elegance around 1860 and gradually transformed, from the 1930s onwards, into the criminal scene known as Boca do Lixo. Strolling through images from that time, the filmmaker explores the architecture of the mansions of the coffee barony, portraying the prosperity of the families that lived there in the past, many of them ruined after the world economic crisis of 1929.`,
}

import { SceneModel } from "../models/scene-model";
import image from "../assets/01-se/texture-compressed.jpg";
import jsonData from "../assets/01-se/data.json";
import { FooterItemButton } from '../components/footer-info'

export const name = 'se';
export const sceneSe = new SceneModel(name)
  .withImagePreload([image.src])
  .withMaterialFn(() => sceneSe.materials[image.src])

export const Ui = ({eventEmitter}) => {
  const { videos } = jsonData.data;

  const handleInfoClick = () => {
    eventEmitter.emit('INFO', null)
  }

  const handleMainVideoClick = () => {
    eventEmitter.emit('MAIN_VIDEO', videos[0])
  }
  
  const handleDocVideoClick = () => {
    eventEmitter.emit('DOC_VIDEO', videos[1])
  }
  
  const handleNavBackClick = () => {
    eventEmitter.emit('NAV_BACK_HOME')
  }

  return (
    <>
      <FooterItemButton onClick={handleMainVideoClick}>
        {'>'} Assista / Watch - SÉ: Urbe-doc
      </FooterItemButton>
      <FooterItemButton onClick={handleDocVideoClick}>
        {'>'} Assista / Watch - SÉ: Planificação e apagamento / Planification and erasure
      </FooterItemButton>
      <FooterItemButton onClick={handleInfoClick}>
        + Info
      </FooterItemButton>
      <FooterItemButton onClick={handleNavBackClick}>
        {'<'} voltar  / back to IN-PLAZA
      </FooterItemButton>
    </>
  )
}

export const info = {
  pt: `Um retrato da vida e da rotina da população que frequentava a Praça da Sé, da situação do transporte público e do crescimento da região central de São Paulo, mostrando também a implosão do Edifício Mendes Caldeira, de 30 andares, no local onde seria iniciada a construção da Estação Sé do Metrô em 1975.`,
  en: `A portrait of the daily life of Sé Square's regulars, the situation of public transport and the growth of the central region of São Paulo, also showing the implosion of the 30-story Mendes Caldeira Building, in the very place where the construction of the Sé Metro Station would begin in 1975.`,
}

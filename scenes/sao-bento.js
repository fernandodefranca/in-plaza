import { SceneModel } from "../models/scene-model";
import image from "../assets/02-sao-bento/texture-compressed.jpg";
import jsonData from "../assets/02-sao-bento/data.json";
import { FooterItemButton } from '../components/footer-info'

export const name = 'sao-bento';
export const sceneSaoBento = new SceneModel(name)
  .withImagePreload([image.src])
  .withMaterialFn(() => sceneSaoBento.materials[image.src])

export const Ui = ({eventEmitter}) => {
  const { videos } = jsonData.data;

  const handleInfoClick = () => {
    eventEmitter.emit('INFO', null)
  }

  const handleMainVideoClick = () => {
    eventEmitter.emit('MAIN_VIDEO', videos[0])
  }
  
  const handleDocVideoClick = () => {
    eventEmitter.emit('DOC_VIDEO', videos[1])
  }
  
  const handleNavBackClick = () => {
    eventEmitter.emit('NAV_BACK_HOME')
  }

  return (
    <>
      <FooterItemButton onClick={handleMainVideoClick}>
        {'>'} Assista / Watch - SÃO BENTO: Urbe-doc
      </FooterItemButton>
      <FooterItemButton onClick={handleDocVideoClick}>
        {'>'} Assista / Watch - SÃO BENTO: Planificação e apagamento / Planification and erasure
      </FooterItemButton>
      <FooterItemButton onClick={handleInfoClick}>
        + Info
      </FooterItemButton>
      <FooterItemButton onClick={handleNavBackClick}>
        {'<'} voltar  / back to IN-PLAZA
      </FooterItemButton>
    </>
  )
}

export const info = {
  pt: `A história do Edifício Martinelli, o primeiro arranha-céu de São Paulo, inaugurado em 1929, através de registros de depoimentos de seus últimos moradores, que tiveram que se mudar após a interdição do prédio pela Prefeitura do Município de São Paulo em 1975. O documentário ilustra a variedade de tipos humanos e de estabelecimentos comerciais que existiam dentro do tradicional edifício.`,
  en: `The history of the Martinelli Building, the first skyscraper in São Paulo, inaugurated in 1929, through testimonies from its last residents, who had to move after the building was interdicted by the Municipality of São Paulo in 1975. The documentary illustrates the variety of human types and commercial establishments that existed within the traditional building.`,
}

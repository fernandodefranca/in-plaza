import { SceneModel } from "../models/scene-model";
import image from "../assets/05-urbanismo/texture-compressed.jpg";
import jsonData from "../assets/05-urbanismo/data.json";
import { FooterItemButton } from '../components/footer-info';

export const name = 'urbanismo';
export const sceneUrbanismo = new SceneModel(name)
  .withImagePreload([image.src])
  .withMaterialFn(() => sceneUrbanismo.materials[image.src])

export const Ui = ({eventEmitter}) => {
  const { videos } = jsonData.data;

  const handleInfoClick = () => {
    eventEmitter.emit('INFO', null)
  }

  const handleMainVideoClick = () => {
    eventEmitter.emit('MAIN_VIDEO', videos[0])
  }
  
  const handleDocVideoClick = () => {
    eventEmitter.emit('DOC_VIDEO', videos[1])
  }
  
  const handleNavBackClick = () => {
    eventEmitter.emit('NAV_BACK_HOME')
  }

  return (
    <>
      <FooterItemButton onClick={handleMainVideoClick}>
        {'>'} Ouça / Listen - LANDSAT
      </FooterItemButton>
      <FooterItemButton onClick={handleDocVideoClick}>
        {'>'} Assista / Watch - LANDSAT: Planificação e apagamento / Planification and erasure
      </FooterItemButton>
      <FooterItemButton onClick={handleInfoClick}>
        + Info
      </FooterItemButton>
      <FooterItemButton onClick={handleNavBackClick}>
        {'<'} voltar  / back to IN-PLAZA
      </FooterItemButton>
    </>
  )
}

export const info = {
  pt: `O audiovisual institucional de 1986 mostra o início do processo de digitalização de documentos, primeiro com o uso do processamento gráfico, depois com o sensoriamento remoto e as imagens satélites (Landsat). O crescimento desordenado da Grande São Paulo impactou na utilização de tecnologia de ponta pelo Setor de Processamento Gráfico, base para o planejamento urbano.`,
  en: `The institutional audiovisual of 1986 shows the beginning of the document digitization process, first with the use of graphic processing, then with remote sensing and satellite images (Landsat). The disorderly growth of Greater São Paulo impacted the use of cutting-edge technology by the Graphic Processing Sector, the basis for urban planning.`,
}

import { SceneModel } from "../models/scene-model";
import image from "../assets/04-transportes/texture-compressed.jpg";
import jsonData from "../assets/04-transportes/data.json";
import { FooterItemButton } from '../components/footer-info';

export const name = 'transportes';
export const sceneTransportes = new SceneModel(name)
  .withImagePreload([image.src])
  .withMaterialFn(() => sceneTransportes.materials[image.src])

export const Ui = ({eventEmitter}) => {
  const { videos } = jsonData.data;

  const handleInfoClick = () => {
    eventEmitter.emit('INFO', null)
  }

  const handleMainVideoClick = () => {
    eventEmitter.emit('MAIN_VIDEO', videos[0])
  }
  
  const handleDocVideoClick = () => {
    eventEmitter.emit('DOC_VIDEO', videos[1])
  }
  
  const handleNavBackClick = () => {
    eventEmitter.emit('NAV_BACK_HOME')
  }

  return (
    <>
      <FooterItemButton onClick={handleMainVideoClick}>
        {'>'} Assista / Watch - TRANSPORTES: Urbe-doc
      </FooterItemButton>
      <FooterItemButton onClick={handleDocVideoClick}>
        {'>'} Assista / Watch - TRANSPORTES: Planificação e apagamento / Planification and erasure
      </FooterItemButton>
      <FooterItemButton onClick={handleInfoClick}>
        + Info
      </FooterItemButton>
      <FooterItemButton onClick={handleNavBackClick}>
        {'<'} voltar  / back to IN-PLAZA
      </FooterItemButton>
    </>
  )
}

export const info = {
  pt: `A peça publicitária de 1979 mostra os planos de expansão e as obras para reformulação e ampliação da oferta dos transportes metropolitanos da Empresa Metropolitana de Transportes Urbanos de São Paulo S/A - EMTU-SP, criada para ser a gestora de operação, planejamento e expansão de transportes integrados, que incluem ônibus municipais, ônibus metropolitanos, corredores de trólebus, trens e metrôs.`,
  en: `The advertising piece from 1979 shows the expansion plans and the works to reformulate and expand the offer of metropolitan transport of Empresa Metropolitana de Transportes Urbanos de São Paulo S/A - EMTU-SP, created to be the manager of operation, planning and expansion. of integrated transport, which include municipal buses, metropolitan buses, trolleybus corridors, trains and subways.`,
}

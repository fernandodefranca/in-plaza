import { SceneModel } from "../models/scene-model";
import image from "../assets/00-cinemateca/texture-compressed.jpg";
import audio from "../assets/00-cinemateca/audio-loop-compressed.m4a";
import { makeCirclePoints } from "../utilities/make-circle-points";
import { FooterItemButton, FooterItem } from '../components/footer-info'

const points = makeCirclePoints(0, 0, 9, 6, -59.75); // Degrees

const doorYPos = 1

const doors = { 
  'I': [points[2].x, doorYPos, points[2].y], 
  'II': [points[3].x, doorYPos, points[3].y], 
  'III': [points[4].x, doorYPos, points[4].y], 
  'IV': [points[5].x, doorYPos, points[5].y], 
  'V': [points[0].x, doorYPos, points[0].y], 
  'VI': [points[1].x, doorYPos, points[1].y],
}

export const name = 'cinemateca';
export const sceneCinemateca = new SceneModel(name)
  .withOnBeforeStart((entities)=> {
    // Set initial rotation
    entities.mesh.rotation.y = -1.565; // Radians
    return entities;
  })
  .withImagePreload([image.src])
  .withAudioPreload(audio)
  .withMaterialFn(() => sceneCinemateca.materials[image.src])
  .withHotspot("se", ...doors['I'], null, "I")
  .withHotspot("saoBento", ...doors['II'], null, "II")
  .withHotspot("camposEliseos", ...doors['III'], null, "III")
  .withHotspot("transportes", ...doors['IV'], null, "IV")
  .withHotspot("urbanismo", ...doors['V'], null, "V")
  .withHotspot("creditos", ...doors['VI'], null, "VI")
  .withLoop((entities) => {
    const elapsedTime = entities.clock.getElapsedTime();
    entities.mesh.rotation.x = Math.sin(elapsedTime) * 0.001;
    entities.mesh.rotation.z = Math.sin(elapsedTime) * -0.001;
  });

export const Ui = ({eventEmitter}) => {
  const handleInfoClick = () => {
    eventEmitter.emit('INFO', null)
  }

  return (
    <>
      <FooterItem>IN-PLAZA</FooterItem>
      <FooterItemButton onClick={handleInfoClick}>
        + Info
      </FooterItemButton>
    </>
  )
}

export const info = {
  pt: `Escolhendo a edição audiovisual e a trilha sonora como agente modificador a ser testado elasticamente dentro de seus limites, IN-PLAZA confronta imagem, documento, registro, política e realidade a partir de fragmentos de documentários, peças publicitárias e registros levantados pela Empresa Paulista de Planejamento Metropolitano (EMPLASA). A EMPLASA, instituição criada em 1975 pelo Governo do Estado de São Paulo para mapear os problemas urbanos da região, atuou principalmente nas décadas de 70 e 80 como agência de pesquisa de informações territoriais e auxiliando no planejamento e projetos de infraestrutura. Construindo novas trilhas sonoras e colagem audiovisual digital para desconstruir o discurso burocrático original desses materiais, IN-PLAZA busca novos olhares críticos sobre o caótico tecido social urbano de uma megacidade que cresce sobre seu próprio passado obliterado.`,
  en: `While choosing audiovisual editing and soundtrack as a modifying agent to be tested elastically within its limits, IN-PLAZA confronts image, document, archive, politics and reality from fragments of documentaries, ads and documents created by Empresa Paulista de Planejamento Metropolitan (EMPLASA). EMPLASA, an institution created in 1975 by the Government of the State of São Paulo in order to map the urban problems in the region, operated mainly in the 70s and 80s as an agency for researching territorial information and assisting in planning and infrastructure projects. By building new soundtracks and digital audiovisual collage to deconstruct the original bureaucratic discourse of these materials, IN-PLAZA seeks new critical perspectives on the chaotic urban social fabric of a megacity that grows over its own obliterated past.`,
}

import * as cinemateca from './cinemateca';
import * as camposEliseos from './campos-eliseos';
import * as saoBento from './sao-bento';
import * as se from './se';
import * as transportes from './transportes';
import * as urbanismo from './urbanismo';
import * as creditos from './creditos';

export default {
  [cinemateca.name]: cinemateca,
  [camposEliseos.name]: camposEliseos,
  [saoBento.name]: saoBento,
  [se.name]: se,
  [transportes.name]: transportes,
  [urbanismo.name]: urbanismo,
  [creditos.name]: creditos,
}
import { useEffect } from 'react';
import styled from 'styled-components';

import { SceneModel } from "../models/scene-model";
import image from "../assets/06-creditos/texture-compressed.jpg";
import { FooterItemButton, FooterItem } from '../components/footer-info'

export const name = 'creditos';
export const sceneCreditos = new SceneModel(name)
  .withImagePreload([image.src])
  .withMaterialFn(() => sceneCreditos.materials[image.src])

export const Ui = ({eventEmitter}) => {
  const handleNavBackClick = () => {
    eventEmitter.emit('NAV_BACK_HOME')
  }

  const handleInfoClick = () => {
    eventEmitter.emit('INFO', null)
  }

  return (
    <>
      <FooterItemButton onClick={handleInfoClick}>
        CRÉDITOS / CREDITS
      </FooterItemButton>
      <FooterItemButton onClick={handleNavBackClick}>
        {'<'} voltar  / back to IN-PLAZA
      </FooterItemButton>
    </>
  )
}

export const CreditsWrapper = styled.div`
  h2, p {
    font-size: 1rem;
    padding: 0;
    margin: 0;
  }
  p + h2 {
    margin-top: 1rem
  }
`

export const info = () => (
  <CreditsWrapper>
    <h2>Equipe / <i>Crew</i>:</h2>
    <p><b>Direção / <i>Direction</i></b> - Marcio Miranda Perez, Nivaldo Godoy</p>
    <p><b>Pesquisa de filmes e documentos / <i>Film and document research</i></b> - Marcio Miranda Perez, Nivaldo Godoy</p>
    <p><b>Edição / <i>Editing</i></b> - Marcio Miranda Perez</p>
    <p><b>Trilha sonora original / <i>Original soundtrack</i></b> - Nivaldo Godoy</p>
    <p><b>Panoramas 360º e colagens audiovisuais / <i>360º panoramas and audiovisual collages</i></b> - Nivaldo Godoy</p>
    <p><b>Mixagem e masterização / <i>Mixing and mastering</i></b> - István (Linchen Ideias Sonoras)</p>
    <p><b>Suporte tecnológico e desenvolvimento do aplicativo / <i>Technological support and application development</i></b> - Fernando de França (Kontakt)</p>
    <p><b>Assessoria de Imprensa / <i>Press office</i></b> - Kelly dos Santos (Mídia Pente Fino)</p>
    
    <h2>Filmes / <i>Films</i>:</h2>
    <p>PRAÇA DA SÉ (1974) - dir. Nilce Tranjan</p>
    <p>RUA SÃO BENTO, 405 (1976) - dir. Ugo Giorgetti</p>
    <p>CAMPOS ELÍSEOS (1973) - dir. Ugo Giorgetti</p>
    <p>TRANSPORTES (1979) - EMTU</p>
    <p>GEOPROCESSAMENTO APLICADO AO PLANEJAMENTO (1986) - EMPLASA</p>
    
    <h2>Agradecimentos / <i>Thanks</i>:</h2>
    <p>Nilce Tranjan, Ugo Giorgetti</p>    
  </CreditsWrapper>
)
import gsap from "gsap";

export function fadeInMesh(entities) {
  return new Promise((resolve) => {
    const { mesh } = entities;

    gsap.to(mesh.material, {
      duration: 1,
      opacity: 1,
      delay: 0.5,
      onComplete: () => resolve(entities),
    });

    return entities;
  });
}

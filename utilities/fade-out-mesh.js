import gsap from "gsap";

export function fadeOutMesh(entities, duration = 1) {
  const { mesh } = entities;

  if (duration === 0) {
    mesh.material.opacity = 0;
    return Promise.resolve(entities);
  }

  return new Promise((resolve) => {
    gsap.to(mesh.material, {
      duration,
      opacity: 0,
      delay: 0,
      onComplete: () => resolve(entities),
    });

    return entities;
  });
}

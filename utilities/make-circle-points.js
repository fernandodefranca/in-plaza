export function makeCirclePoints(centerX, centerY, circleRadius, totalPoints, startAngle = 0, arc = 360, pointDirection = "clockwise") {
  const mpi = Math.PI/180;
  let startRadians = startAngle * mpi;
  
  let incrementAngle = arc/totalPoints;
  let incrementRadians = incrementAngle * mpi;
  
  if(arc<360) {
    incrementAngle = arc/(totalPoints-1);
    incrementRadians = incrementAngle * mpi;
  }
  
  const pts = [];
  
  while(totalPoints--) {
    const x = centerX + Math.sin(startRadians) * circleRadius;
    const y = centerY + Math.cos(startRadians) * circleRadius;
    const pt = {x, y};
    pts.push(pt);
    
    if(pointDirection==='COUNTERCLOCKWISE') {
      startRadians += incrementRadians;
    } else {
      startRadians -= incrementRadians;
    }
  }
  
  return pts;
}

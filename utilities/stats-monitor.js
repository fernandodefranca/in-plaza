import Stats from "three/examples/jsm/libs/stats.module.js";
import { config } from "../config";
import { isServer } from '../utilities/is-server'

export const stats = isServer() ? {} : new Stats();
if (config.isDebug && !isServer()) {
  document.body.appendChild(stats.dom);
}
